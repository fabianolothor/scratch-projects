# scratch-projects

You can access all my Scratch projects at: https://scratch.mit.edu/users/FabianoLothor/projects/

## CAT RUNNER

It's an infinite runner where the user can move the character using the keyboard to avoid the enemies and get more points.

> You can play at: https://scratch.mit.edu/projects/625700237/

- **GAME KEYS**
  - `<SPACE>` to start the game
  - `<R>` to restart the game
  - `<M>` to mute the sound
  - `<LEFT-ARROW>` to move left
  - `<RIGHT-ARROW>` to move right
